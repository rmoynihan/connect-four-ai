import numpy as np
from random import random, randint
from scipy import stats
from keras.utils import to_categorical

from game_state import GameState
from dqn_agent import DQNAgent

class GameAgent:
    def __init__(self, name, color, always_random=False):
        # Agent UI
        self.name = name
        self.color = color
        self.always_random = always_random

        # Agent's DQN
        self.dqn = DQNAgent(self, 7)

        # Epsilon settings
        self.epsilon_max = 250
        self.epsilon = self.epsilon_max

        # This agent's GameState object
        self.game_state = None

        # Numpy arrays that hold GameState snapshots
        self.state_old = None
        self.state_new = None

        # Last action
        self.last_action = None

    def turn_start(self):
        self.state_old = self.game_state.get_state()

    def make_move(self):
        # # Determine random action or predicted action
        # if randint(0, self.epsilon_max) < self.epsilon or self.always_random:
        #     # Random
        #     action = to_categorical(randint(0,1), num_classes=7)
        # else:
        #     # Predicted
        #     prediction = self.dqn.model.predict(self.state_old.reshape((1, 42)))
        #     action = to_categorical(np.argmax(prediction[0]), num_classes=7)

        action = randint(1,7) - 1
        self.game_state.game_map.add_piece(self, action)
        self.last_action = action

    def turn_end(self):
        self.state_new = self.game_state.get_state()

        # Check if this agent won the game
        won = 0 #check_won_game()

        # Online training and memory storage
        if not self.always_random:
            reward = self.dqn.set_reward(won)
            self.dqn.train_short_memory(self.state_old,  self.last_action, reward, self.state_new)
            self.dqn.remember(self.state_old, self.last_action, reward, self.state_new)

    # Actions to be performed at the conclusion of each game
    def end_of_game(self):
        if not self.always_random:
            self.dqn.replay_memories()

        # Reset epsilon
        self.epsilon -= 1
