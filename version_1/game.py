import pygame
import sys
import time
import random
import numpy as np

from game_map import GameMap
from game_state import GameState

class Game:
    # Initialize the game environment
    def __init__(self, agents):
        pygame.init()

        # VARS
        self.display_width = 800
        self.display_height = 600
        self.bkgr_color = (255, 255, 255)
        self.agents = agents

        # Pygame meta
        self.game_display = pygame.display.set_mode((self.display_width, self.display_height))
        pygame.display.set_caption("DeepQ C4")

    def setup(self):
        # Draw game map
        self.game_display.fill(self.bkgr_color)
        self.game_map = GameMap(self.game_display, self.display_width, self.display_height)

        for agent in self.agents:
            agent.game_state = GameState(self.game_map, agent)

    # Start the game loop
    def play(self):
        game_over = False

        # Game loop
        while not game_over:
            # Game logic
            game_over = self.step()

            # Check exit
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    game_over = True
                    return True

        return False

    # Each round of the game
    def step(self):
        for agent in self.agents:
            # agent.turn_start()
            agent.make_move()
            # agent.turn_end()

            # Refresh the map
            self.game_map.refresh_map()
            self.game_map.to_console()

            time.sleep(2)


        # Keep the game going
        return False

    def exit(self):
        # Exit game
        pygame.display.quit()
        pygame.quit()
        sys.exit()
