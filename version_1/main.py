from game_agent import GameAgent
from game import Game

# Create game agents
agents = [ GameAgent('B', (0,0,0)), GameAgent('R', (255,0,0)) ]

# Create game and start
game = Game(agents)

user_exit = False
game_num = 0
while not user_exit:
    print("\n", '*' * 25)
    print("GAME #" + str(game_num))
    print()

    game.setup()

    user_exit = game.play()

    # Re-fit agents
    for agent in agents:
        agent.end_of_game()

    # Play again
    game_num += 1
