import numpy as np
from pprint import pprint

class GameState:
    def __init__(self, game_map, owner):
        self.game_map = game_map
        self.owner = owner

        self.state = [[None for i in range(6)] for j in range(6)]

    # Check for updates in the game grid
    def update(self):
        for i in range(7):
            for j in range(6):
                if self.game_map.grid[i][j] != None:
                    self.state[i][j] = 0
                    if self.game_map.grid[i][j] == self.owner:
                        self.state[i][j] = 1
                    else:
                        self.state[i][j] = -1

    # Get state as numpy array
    def get_state(self):
        # Make sure state is current
        self.update()

        # Convert 2D matrix to 1D list
        state = []
        for row in self.state:
            for cell in row:
                state.append(cell)

        # Return as a numpy array
        return np.array(state)
