import pygame
import sys
from game_agent import GameAgent

class GameMap:
    def __init__(self, game_display, display_width, display_height):
        self.game_display = game_display
        self.display_width = display_width
        self.display_height = display_height

        # Instantiate the game matrix
        self.grid = [[None for i in range(7)] for j in range(6)]

        self.refresh_map()

    # Draw the grid lines
    def draw_lines(self):
        for n in range(0, 7):
            x = (self.display_width/7) * (n + 1)
            y1 = 0
            y2 = self.display_height
            pygame.draw.line(self.game_display, (0,0,0), (x, y1), (x, y2))

        for n in range(0, 6):
            y = (self.display_height/6) * (n + 1)
            x1 = 0
            x2 = self.display_width
            pygame.draw.line(self.game_display, (0,0,0), (x1, y), (x2, y))

    # Add a piece to the game board
    def add_piece(self, agent, col):
        # Get the first empty cell in the column
        # row = 0
        # while row <= 5:
        #     if self.grid[row][col] != None:
        #         break
        #     row += 1
        # row -= 1
        #
        # # Make sure column isn't full
        # if row >= 0:
        #     self.grid[row][col] = agent
        self.grid[0][col] = agent


    # Draw out the game pieces
    def draw_pieces(self):
        for i in range(6):
            for j in range(7):
                if self.grid[i][j] != None:
                    color = self.grid[i][j].color
                    center_x = 133
                    center_y = 266
                    rad = int(self.display_width/24)
                    pygame.draw.circle(self.game_display, color, (center_x, center_y), rad)

    # Refresh the game screen
    def refresh_map(self):
        # Erase screen contents
        self.game_display.fill((255,255,255))

        self.draw_lines()

        self.draw_pieces()

        pygame.display.update()

    def to_console(self):
        print()
        print('*' * 20)
        for row in self.grid:
            print()
            for col in row:
                print(0 if col == None else 1, end='  ')
